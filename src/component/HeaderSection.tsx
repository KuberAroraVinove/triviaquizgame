import React from 'react';
import { SafeAreaView, StyleSheet, Text, StatusBar } from 'react-native';
import { Header } from 'react-native/Libraries/NewAppScreen';

const HeaderSection = () => {
    return (
        <SafeAreaView >
            <Header />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    textstyle: {
        textAlign: 'center',
        fontSize: 18,
        color: 'black'
    }
});

export default HeaderSection;