import React, { useEffect, useReducer, useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View, Dimensions, ActivityIndicator, TextInput, TouchableOpacity, Alert } from 'react-native';
import { getQuestionAir } from '../service';
import { QuestionType } from '../types/type';

const emptyQuestionType: QuestionType = {
    category: '',
    correct_answer: '',
    difficulty: '',
    incorrect_answer: [],
    question: '',
    type: ''
}

const QuestionAirSection = () => {
    const [questionAirData, setQuestionAirData] = useState<QuestionType>(emptyQuestionType);
    const [quesNum, setQuesNum] = useState<number>(1);
    const [marksStatus, setMarksStatus] = useState<number>(0);
    const [userAnswer, setUserAnswer] = useState<string>('');
    const [userAnswerStatus, setUserAnswerStatus] = useState<string>('');

    useEffect(() => {
        startQuiz()
    }, [])
    const startQuiz = () => {
        getQuestionAir((data: any) => {
            setQuestionAirData(data.results[0]);
        })
    }
    const userAnswerCheck = () => {
        if (userAnswer) {
            {
                questionAirData.correct_answer.toLowerCase().trim() === userAnswer.toLowerCase().trim() ? (setUserAnswerStatus('Correct'),
                    setMarksStatus(marksStatus + 1)) : setUserAnswerStatus('Incorrect')
            }
        }
        else {
            Alert.alert('Answer Field Cant be empty')
        }

    }
    return (
        <SafeAreaView style={{ height: Dimensions.get('window').height }}>
            {questionAirData === emptyQuestionType ?
                <ActivityIndicator size='large' color='white' style={{ marginTop: 250 }} /> :
                <View style={{ padding: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                        <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>Question : {quesNum}/10</Text>
                        <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>Marks : {marksStatus}</Text>

                    </View>
                    <View style={{ borderWidth: 0, borderColor: 'white', marginTop: 30, borderRadius: 10, padding: 10 }}>
                        <Text style={{ marginTop: 5, fontSize: 20, fontWeight: 'bold', color: 'white' }}>Q{quesNum}: {questionAirData.question}</Text>
                    </View>
                    <View style={[{ borderWidth: 2, marginTop: 15, borderRadius: 10, padding: 5, borderColor: 'white' }]}>
                        <TextInput
                            value={userAnswer}
                            placeholderTextColor="white"
                            placeholder="Enter your answer here"
                            onChangeText={setUserAnswer}
                        />
                    </View>
                    {userAnswerStatus !== '' &&
                        <View style={{ marginTop: 10 }}>
                            <Text style={[userAnswerStatus === 'Correct' ? { color: 'green' } : { color: 'red' }, { fontSize: 20, fontWeight: 'bold' }]}>{userAnswerStatus}</Text>
                            {userAnswerStatus === 'Incorrect' ? <Text style={{ color: 'green', fontSize: 20, fontWeight: 'bold', marginTop: 5 }}>Correct Answer : {questionAirData.correct_answer}</Text> : ''}
                        </View>}
                    <View>
                        {userAnswerStatus === '' && <TouchableOpacity
                            style={{ ...styles.touchable, height: margh40 }} onPress={() => userAnswerCheck()}>
                            <Text style={{ ...styles.sectionTitle, fontFamily: 'robotobold', fontSize: 18 }} >
                                {`Submit`.toUpperCase()}
                            </Text>
                        </TouchableOpacity>}
                        {quesNum < 10 && userAnswerStatus !== '' && <TouchableOpacity
                            style={{ ...styles.touchable, height: margh40 }}
                            onPress={() => {
                                startQuiz();
                                setQuesNum(quesNum + 1);
                                setUserAnswer('');
                                setUserAnswerStatus('');
                                setQuestionAirData(emptyQuestionType)
                            }

                            }>
                            <Text style={{ ...styles.sectionTitle, fontFamily: 'robotobold', fontSize: 18 }} >
                                {`Next`.toUpperCase()}
                            </Text>
                        </TouchableOpacity>}
                        {quesNum === 10 && userAnswerStatus !== '' && <TouchableOpacity
                            style={{ ...styles.touchable, height: margh40 }}
                            onPress={() => {
                                Alert.alert(`You Scored : ${marksStatus}`)
                            }

                            }>
                            <Text style={{ ...styles.sectionTitle, fontFamily: 'robotobold', fontSize: 18 }} >
                                {`Show Result`.toUpperCase()}
                            </Text>
                        </TouchableOpacity>}
                    </View>
                </View>
            }
        </SafeAreaView>
    );
};

var marg = (20 * Dimensions.get('window').width) / 400;
var marg10 = (10 * Dimensions.get('window').width) / 400;
var bmarg = (40 * Dimensions.get('window').width) / 400;
var marg = (20 * Dimensions.get('window').width) / 400;
var marg10 = (30 * Dimensions.get('window').width) / 400;
var bmarg = (40 * Dimensions.get('window').width) / 400;
var margh5 = (5 * Dimensions.get('window').height) / 774;
var margh40 = (50 * Dimensions.get('window').height) / 774;


const styles = StyleSheet.create({
    textstyle: {
        textAlign: 'center',
        fontSize: 18,
        color: 'black'
    },
    touchable: {
        width: Dimensions.get('window').width - bmarg,
        borderRadius: 10,
        marginTop: marg10,
        marginBottom: marg,
        backgroundColor: '#1c75bc',
        textAlign: 'center',
        alignSelf: 'center'
    },
    sectionTitle: {
        width: Dimensions.get('window').width - bmarg,
        height: margh40,
        fontWeight: "bold",
        fontSize: 16,
        padding: margh5,
        color: '#FFFFFF',
        fontFamily: 'robotoregular',
        textAlignVertical: 'center',
        alignSelf: 'center',
        textAlign: 'center',
    },
});

export default QuestionAirSection;