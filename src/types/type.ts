export type QuestionType = {
    category: string,
    correct_answer: string,
    difficulty: string,
    incorrect_answer: Array<string>,
    question: string,
    type: string
}